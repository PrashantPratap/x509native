using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography;
using Microsoft.Win32.SafeHandles;

namespace TestApp
{
    public static class X509Certificate2ExtensionMethods
    {
        internal enum AcquireCertificateKeyOptions
        {
            None = 0x00000000,
            AcquireOnlyNCryptKeys = 0x00040000,   // CRYPT_ACQUIRE_ONLY_NCRYPT_KEY_FLAG
        }
        [StructLayout(LayoutKind.Sequential)]
        internal struct CRYPT_KEY_PROV_INFO
        {
            [MarshalAs(UnmanagedType.LPWStr)]
            internal string pwszContainerName;
            [MarshalAs(UnmanagedType.LPWStr)]
            internal string pwszProvName;
            internal int dwProvType;
            internal int dwFlags;
            internal int cProvParam;
            internal IntPtr rgProvParam;        // PCRYPT_KEY_PROV_PARAM
            internal int dwKeySpec;
        }
        internal enum CertificateProperty
        {
            KeyProviderInfo = 2,    // CERT_KEY_PROV_INFO_PROP_ID 
            KeyContext = 5,    // CERT_KEY_CONTEXT_PROP_ID
        }
        internal enum ErrorCode
        {
            Success = 0x00000000,       // ERROR_SUCCESS
            MoreData = 0x000000ea,       // ERROR_MORE_DATA
        }
        [DllImport("crypt32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool CertGetCertificateContextProperty(SafeCertContextHandle pCertContext,
                                                                         CertificateProperty dwPropId,
                                                                         [Out, MarshalAs(UnmanagedType.LPArray)] byte[] pvData,
                                                                         [In, Out] ref int pcbData);
        [DllImport("crypt32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool CryptAcquireCertificatePrivateKey(SafeCertContextHandle pCert,
                                                                         AcquireCertificateKeyOptions dwFlags,
                                                                         IntPtr pvReserved,        // void *
                                                                         [Out] out SafeNCryptKeyHandle phCryptProvOrNCryptKey,
                                                                         [Out] out int dwKeySpec,
                                                                         [Out, MarshalAs(UnmanagedType.Bool)] out bool pfCallerFreeProvOrNCryptKey);
        [DllImport("crypt32.dll")]
        internal static extern SafeCertContextHandle CertDuplicateCertificateContext(IntPtr certContext);
        public sealed class SafeCertContextHandle : SafeHandleZeroOrMinusOneIsInvalid
        {
            private SafeCertContextHandle() : base(true)
            {
            }
            [DllImport("crypt32.dll")]
            [return: MarshalAs(UnmanagedType.Bool)]
            private static extern bool CertFreeCertificateContext(IntPtr pCertContext);
            protected override bool ReleaseHandle()
            {
                return CertFreeCertificateContext(handle);
            }
        }
        internal static bool HasCertificateProperty(SafeCertContextHandle certificateContext,
                                                   CertificateProperty property)
        {
            byte[] buffer = null;
            int bufferSize = 0;
            bool gotProperty = CertGetCertificateContextProperty(certificateContext,
                                                                                     property,
                                                                                     buffer,
                                                                                     ref bufferSize);
            return gotProperty ||
                   (ErrorCode)Marshal.GetLastWin32Error() == ErrorCode.MoreData;
        }

        internal static T GetCertificateProperty<T>(SafeCertContextHandle certificateContext,
                                                   CertificateProperty property) where T : struct
        {
            byte[] rawProperty = GetCertificateProperty(certificateContext, property);

            unsafe
            {
                fixed (byte* pRawProperty = &rawProperty[0])
                {
                    return (T)Marshal.PtrToStructure(new IntPtr(pRawProperty), typeof(T));
                }
            }
        }
        internal static byte[] GetCertificateProperty(SafeCertContextHandle certificateContext,
                                                      CertificateProperty property)
        {
            byte[] buffer = null;
            int bufferSize = 0;
            if (!CertGetCertificateContextProperty(certificateContext,
                                                                       property,
                                                                       buffer,
                                                                       ref bufferSize))
            {
                ErrorCode errorCode = (ErrorCode)Marshal.GetLastWin32Error();
                if (errorCode != ErrorCode.MoreData)
                {
                    throw new CryptographicException((int)errorCode);
                }
            }
            buffer = new byte[bufferSize];
            if (!CertGetCertificateContextProperty(certificateContext,
                                                                       property,
                                                                       buffer,
                                                                       ref bufferSize))
            {
                throw new CryptographicException(Marshal.GetLastWin32Error());
            }
            return buffer;
        }

        public static bool HasCngKey(this X509Certificate certificate)
        {
            using (SafeCertContextHandle certContext = certificate.GetCertificateContext())
            {
                if (HasCertificateProperty(certContext,
                                                      CertificateProperty.KeyProviderInfo))
                {
                    CRYPT_KEY_PROV_INFO keyProvInfo =
                        GetCertificateProperty<CRYPT_KEY_PROV_INFO>(certContext, CertificateProperty.KeyProviderInfo);
                    return keyProvInfo.dwProvType == 0;
                }
                else

                {
                    return false;
                }
            }
        }
        internal static SafeCertContextHandle DuplicateCertContext(IntPtr context)
        {
            return CertDuplicateCertificateContext(context);
        }
        public static SafeCertContextHandle GetCertificateContext(this X509Certificate certificate)
        {
            SafeCertContextHandle certContext = DuplicateCertContext(certificate.Handle);
            // Make sure to keep the X509Certificate object alive until after its certificate context is

            // duplicated, otherwise it could end up being closed out from underneath us before we get a

            // chance to duplicate the handle.
            GC.KeepAlive(certificate);
            return certContext;
        }
        public static CngKey GetCngPrivateKey(this X509Certificate2 certificate)
        {
            if (!certificate.HasPrivateKey || !certificate.HasCngKey())
            {
                return null;
            }
            using (SafeCertContextHandle certContext = certificate.GetCertificateContext())
            using (SafeNCryptKeyHandle privateKeyHandle = AcquireCngPrivateKey(certContext))
            {
                return CngKey.Open(privateKeyHandle, CngKeyHandleOpenOptions.None);
            }
        }
        internal static SafeNCryptKeyHandle AcquireCngPrivateKey(SafeCertContextHandle certificateContext)
        {
            bool freeKey = true;
            SafeNCryptKeyHandle privateKey = null;

            try
            {
                int keySpec = 0;
                if (!CryptAcquireCertificatePrivateKey(certificateContext,
                                                                           AcquireCertificateKeyOptions.AcquireOnlyNCryptKeys,
                                                                           IntPtr.Zero,
                                                                           out privateKey,
                                                                           out keySpec,
                                                                           out freeKey))
                {
                    throw new CryptographicException(Marshal.GetLastWin32Error());
                }
                return privateKey;
            }
            finally
            {
                // If we're not supposed to release they key handle, then we need to bump the reference count
                // on the safe handle to correspond to the reference that Windows is holding on to.  This will
                // prevent the CLR from freeing the object handle.
                // 
                // This is certainly not the ideal way to solve this problem - it would be better for
                // SafeNCryptKeyHandle to maintain an internal bool field that we could toggle here and
                // have that suppress the release when the CLR calls the ReleaseHandle override.  However, that
                // field does not currently exist, so we'll use this hack instead.
                if (privateKey != null && !freeKey)
                {
                    bool addedRef = false;
                    privateKey.DangerousAddRef(ref addedRef);
                }
            }
        }
    }
}
